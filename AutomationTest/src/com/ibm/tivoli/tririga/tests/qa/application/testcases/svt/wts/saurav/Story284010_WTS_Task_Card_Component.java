package com.ibm.tivoli.tririga.tests.qa.application.testcases.svt.wts.saurav;

import com.ibm.tivoli.tririga.automation.framework.BaseTestv2;
import com.ibm.tivoli.tririga.automation.framework.LoginPage;
import com.ibm.tivoli.tririga.automation.framework.application.person.Person;
import com.ibm.tivoli.tririga.automation.framework.application.person.PersonObjects;
import com.ibm.tivoli.tririga.automation.framework.application.utils.v2.NavigationUtils;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.WORKTASK_PRIORITY;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.data.WorkTaskData;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.util.WorkTaskUXUtilV2;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.util.WorkTaskUtilV2;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.web.WorkTask;
import com.ibm.tivoli.tririga.automation.framework.services.ScriptTool;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.action.ShiftActionTypes;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.app.content.ShiftAppPageContentTypes;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.page.ShiftPageContentTypes;
import com.ibm.tivoli.tririga.automation.webdriver.framework.TririgaUxApp;
import com.ibm.tivoli.tririga.automation.webdriver.framework.shiftpolymer.ShiftConstants;
import com.ibm.tivoli.tririga.automation.webdriver.framework.shiftpolymer.UxPriority;
import com.ibm.tivoli.tririga.automation.webdriver.framework.shiftpolymer.verification.ShiftVerifications;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.CommandOn;

import org.testng.annotations.Test;

public class Story284010_WTS_Task_Card_Component extends BaseTestv2 {
	public final static String sScriptTestArea = "Work Task Services";
	public final static String sScriptDescription = "Phoenix release: ";
	public final static String sScriptTCDBID = "";
	public static String sScriptTitle = "Worktask Services - Task Card Components";
	protected static final String TASK_NAME = "WORKTASK_COMPONENTS_DISPLAY";
	protected static final String TASK_TYPE = "Move";
	protected static final String TASK_LOCATION = "SL TEST Building";
	protected static final String USER_FULLNAME = "Service Manager2";

	public final String sScriptTCDBDB = "<ENTER TCDB DATABASE FILENAME HERE dirsec.nsf>";
	public String logFileName;

	/**
	 * Always specify the group
	 */
	@Test(groups = {
			"WTS Large Screen"
	})

	/**
	 * Name of the Method: Task_Card_Components()
	 * Tester:
	 * Date:
	 * Description: Following method Task_Card_Components(), creates a work task in classic, logs into UX app,
	 * select the certain work task created in classic and then starts the timer, stops the time,
	 * verifies the location, status of the work task in card view.
	 */

	public void Task_Card_Components() {
		// BEGIN: Do not alter code within this block ***
		ScriptTool.runMainSetup(sScriptTestArea, sScriptTitle, sScriptDescription, sScriptTCDBID, this.logFileName);
		// END: Do not alter code within this block ***

		this.runFrameworkTest(new CommandOn<LoginPage>() {

			@Override

			public void executeOn(final LoginPage loginPage) {
				final Person WorktaskCreator = PersonObjects.WTS_managerB;
				final WorkTaskData WORKTASK = new WorkTask();
				final String Id = null;
				WORKTASK.setId(Id);
				WORKTASK.setName(TASK_NAME);
				WORKTASK.setType(TASK_TYPE);
				WORKTASK.setPriority(WORKTASK_PRIORITY.HIGH);
				WORKTASK.setPrimaryLocation(TASK_LOCATION);
				WORKTASK.setPeopleResource(WorktaskCreator.getFullName());

				loginPage

						.login(WorktaskCreator.getUsername(), WorktaskCreator.getPassword())
						// Navigate to Main Work Task Page
						.$(NavigationUtils.MAIN_TO_WORK_TASK_PAGE)

						.$(WorkTaskUtilV2.CREATE_WORKTASK(WORKTASK)) // Create Work task

						.mainPage()
						.logout() // Logout out from classic app

						.shiftAppLogin(WorktaskCreator.getUsername(), WorktaskCreator.getPassword(),
								TririgaUxApp.WORKTASK_SERVICES)
						.pageContent(ShiftPageContentTypes.SHIFT_DEFAULT)
						.app()
						.$(WorkTaskUXUtilV2.TAB("In Progress"))
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.appPage()
						.action(ShiftActionTypes.actionById("layoutToggleButton", ShiftActionTypes.SHIFT_APP))
						.click()
						// TODO: Revisit
						.field(ShiftConstants.searchField())
						.enter(WORKTASK.getId())
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(0)
						.verify(ShiftVerifications.hasPriority(UxPriority.HIGH))
						.appPage()
						// .section("tricomp-task-priority")
						// .verify(ShiftVerifications.hasIcon("priority-medium"))
						// .appPage()
						// Test_Case_001
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(0)
						.section("tricomp-task-lifecycle")
						.verify(ShiftVerifications.hasAction("ibm:timer-start")) // Verify the Start timer icon
						.appPage()
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(0)
						.verify(ShiftVerifications.contains("Active")) // Verify if the task is active
						// Test_Case_002
						.appPage()
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(0)
						.section("tricomp-task-lifecycle")
						.action(ShiftActionTypes.actionByIcon("ibm:timer-start", ShiftActionTypes.SHIFT_APP)) // Start the timer
						.click()
						// Test_Case_003
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(0)
						.section("tricomp-task-lifecycle")
						.verify(ShiftVerifications.hasAction("ibm:timer-stop")) // Verify the Stop timer icon
						// Test_Case_005
						.appPage()
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(0)
						.verify(ShiftVerifications.contains("Active")) // Verify if the task is active
						.appPage()
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(0)
						.section("tricomp-task-lifecycle")
						.action(ShiftActionTypes.actionByIcon("ibm:timer-stop", ShiftActionTypes.SHIFT_APP)) // Stop the timer
						.click()
						// Test_Case_004
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(0)
						.verify(ShiftVerifications.contains("Active")) // Verify if the status of the work task is active
						// Test_Case_007
						.verify(ShiftVerifications.contains(TASK_LOCATION)) // Verify the location of the work task

						.verify(ShiftVerifications.contains(TASK_NAME)) // Verify the work task name
						// Test_Case_006
						// Verify the Priority of the work task
						.appPage()
						.$(WorkTaskUXUtilV2.COMPLETE_WORKTASK(WORKTASK.getId())) // Complete the work task
						.$(WorkTaskUXUtilV2.CLOSE_WORKTASK(WORKTASK.getId())) // Close the work task

						.logout();

			}
		});
	}
}
