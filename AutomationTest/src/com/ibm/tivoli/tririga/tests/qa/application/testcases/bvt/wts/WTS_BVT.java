package com.ibm.tivoli.tririga.tests.qa.application.testcases.bvt.wts;

import com.ibm.tivoli.tririga.automation.framework.BaseTestv2;
import com.ibm.tivoli.tririga.automation.framework.LoginPage;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.util.WorkTaskUXUtilV2;
import com.ibm.tivoli.tririga.automation.framework.services.ScriptTool;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.action.ShiftActionTypes;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.app.content.ShiftAppPageContentTypes;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.page.ShiftPageContentTypes;
import com.ibm.tivoli.tririga.automation.webdriver.framework.TririgaUxApp;
import com.ibm.tivoli.tririga.automation.webdriver.framework.shiftpolymer.ShiftConstants;
import com.ibm.tivoli.tririga.automation.webdriver.framework.shiftpolymer.verification.ShiftVerifications;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.CommandOn;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.wrappers.AutomationInteger;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.wrappers.AutomationWrappers;

import org.testng.annotations.Test;

public class WTS_BVT extends BaseTestv2 {
	public final static String sScriptTestArea = "Work Task Services";
	public final static String sScriptDescription = "Phoenix release: ";
	public final static String sScriptTCDBID = "";
	public static String sScriptTitle = "Workplace Services - Locate";
	public final String sScriptTCDBDB = "<ENTER TCDB DATABASE FILENAME HERE dirsec.nsf>";
	public String logFileName;

	/**
	 * Always specify the group
	 */
	@Test(groups = {
			"WTS"
	})
	public void test_workTaskServices_bvt() {
		// BEGIN: Do not alter code within this block ***
		ScriptTool.runMainSetup(sScriptTestArea, sScriptTitle, sScriptDescription, sScriptTCDBID, this.logFileName);
		// END: Do not alter code within this block ***

		this.runFrameworkTest(new CommandOn<LoginPage>() {

			@Override
			public void executeOn(final LoginPage loginPage) {
				final AutomationInteger count = AutomationWrappers.createInteger();
				final AutomationInteger totalNumberPSteps = AutomationWrappers.createInteger();
				final AutomationInteger totalNumberRequiredPSteps = AutomationWrappers.createInteger();
				final AutomationInteger totalNumberCompletedPSteps = AutomationWrappers.createInteger();
				final AutomationInteger totalNumberCompletedRequiredPSteps = AutomationWrappers.createInteger();

				loginPage.shiftAppLoginAsDefaultUser(TririgaUxApp.WORKTASK_SERVICES)
						.pageContent(ShiftPageContentTypes.SHIFT_DEFAULT)
						.app()
						.action(ShiftActionTypes.actionById("layoutToggleButton", ShiftActionTypes.SHIFT_APP))
						.click()
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.sortBy("Location")
						.appPage()
						// Search for the work task
						.$(WorkTaskUXUtilV2.TAB("Completed")) // Click on In Progress tab

						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(1) // Select the record
						.click()

						.$(WorkTaskUXUtilV2.UNFOLD_ASSETS_SECTION())
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)// Opening the First Asset record
						.click()
						.verify(ShiftVerifications.contains("Asset Details")) // Verify that the page contains Asset details tab
						.$(WorkTaskUXUtilV2.TAB("Floor Plan")) // Click on Floor Plan tab

						.verify(ShiftVerifications.notContains("Floor plan not available.")) // Verify it is having Floor plan
						.verify(ShiftVerifications.hasAction("ibm-glyphs:incremental-input-add")) // Verify the "+" button has action
						.verify(ShiftVerifications.hasAction("ibm-glyphs:incremental-input-minus"))// Verify the "-" button has action
						// .action(ShiftActionTypes.actionById("layoutToggleButton", ShiftActionTypes.SHIFT_APP))
						// .click()
						// .$(WorkTaskUXUtilV2.TAB("In Progress"))
						// .click()
						// .appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						// .$(WorkTaskUXUtilV2.FILTER_BY("All"))
						// .verify(ShiftVerifications.contains("Sort by:"))
						// .sortBy("Status")
						// .appPage()
						//
						// .action(ShiftActionTypes.actionById("layoutToggleButton", ShiftActionTypes.SHIFT_APP))
						// .click()

						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						// .search("procedure")
						// .$(WorkTaskUXUtilV2.FILTER_BY("All"))
						// .saveCountTo(count)
						//
						// .verify(ShiftVerifications.contains("Planned Start"))
						// .sortDescending("Planned Start")
						//
						// .filterBy("My created tasks")
						// .$(WorkTaskUXUtilV2.FILTER_BY("All"))
						// .sortAscending("Planned Start")
						.search("1038022")
						.row(0)
						// .record(0)
						// .verify(ShiftVerifications.hasIcon("ibm:timer", "priority-low"))
						// .record(0)
						// .verify(ShiftVerifications.hasAction("ibm:timer"))
						// .record(0)
						// .verify(ShiftVerifications.hasNoAction("priority-low"))
						.click()
						.$(WorkTaskUXUtilV2.UNFOLD_TIME_SECTION())
						.action(ShiftActionTypes.actionByIcon("ibm:timer-start", ShiftActionTypes.SHIFT_APP))
						.click()
						.toast()
						.verify(ShiftVerifications.contains("Timer started"))
						.shiftPage()
						.logout()

						// USER 2 LOGIN
						.shiftAppLogin("Svrmgr1", "password", TririgaUxApp.WORKTASK_SERVICES)
						.pageContent(ShiftPageContentTypes.SHIFT_DEFAULT)
						.app()
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.click()
						// work task detail page is displayed

						// Test Case 003 : Verify if there is no record under timelog when the user has not start the task
						.$(WorkTaskUXUtilV2.UNFOLD_TIME_SECTION())
						.action(ShiftActionTypes.actionByIcon("ibm:timer-stop", ShiftActionTypes.SHIFT_APP))
						.click()
						.toast()
						.verify(ShiftVerifications.contains("Error"))
						.shiftPage()
						.logout()

						// user1 login
						.shiftAppLoginAsDefaultUser(TririgaUxApp.WORKTASK_SERVICES)
						.pageContent(ShiftPageContentTypes.SHIFT_DEFAULT)
						.app()
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.search("1038022")
						.row(0)
						.click()
						.$(WorkTaskUXUtilV2.UNFOLD_TIME_SECTION())
						.action(ShiftActionTypes.actionByIcon("ibm:timer-stop", ShiftActionTypes.SHIFT_APP))
						.click()
						.toast()
						.verify(ShiftVerifications.contains("Timer stopped"))

						.$(WorkTaskUXUtilV2.UNFOLD_PROCEDURES_SECTION())
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						// .filterBy("Tasks")
						.search("Asset")
						.row(0)
						.click()
						// Asset Procedure detail page is displayed
						//
						// Once Procedure Details page is displayed

						/**
						 * implement by Preethi toDo
						 * // Should be able to toggle the Hide optional steps? and verify
						 * .field(ShiftConstants.toggleField("Hide optional steps?:"))
						 * .select("Yes")
						 * .field(ShiftConstants.toggleField("Hide optional steps?:"))
						 * .verify(ShiftVerifications.isSelected())
						 * .field(ShiftConstants.toggleField("Hide optional steps?:"))
						 * .select("No")
						 * .field(ShiftConstants.toggleField("Hide optional steps?:"))
						 * .verify(ShiftVerifications.isNotSelected())
						 **/
						// Should be able to access the Procedure Steps List
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						// Should be able to filter
						.filterBy("All")
						// Should be able to return the total count (All, not hide optional steps)
						.saveCountTo(totalNumberPSteps)
						.filterBy("Complete")
						// Should be able to return the total completed count (Completed, not hide optional steps)
						.saveCountTo(totalNumberCompletedPSteps)
						// Should be able to display only required procedure steps
						/**
						 * .field(ShiftConstants.toggleField("Hide optional steps?:"))
						 * .select("Yes")
						 */

						// Should be able to filter
						.filterBy("All")
						// Should be able to return the total required count (All, hide optional steps)
						.saveCountTo(totalNumberRequiredPSteps)
						.filterBy("Complete")
						// Should be able to return the total completed required count (Completed, hide optional steps)
						.saveCountTo(totalNumberCompletedRequiredPSteps)
						// Should be able to interact with each row, which is a ProcedureStepRowPage
						.record(0)
						// Should be able to enter texts
						.field(ShiftConstants.textField("Reading"))
						.enter("2239")
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(0)
						.field(ShiftConstants.textField("Comments"))
						.enter("auto test")
						// Should be able to toggle Complete toggle
						/**
						 * .field(ShiftConstants.toggleField("Complete?:"))
						 * // If toggle is already set to Yes, the below line will not do anything
						 * .select("Yes")
						 * // Should be able to verify the toggle state
						 * .field(ShiftConstants.toggleField("Complete?:"))
						 * .verify(ShiftVerifications.isNotSelected())
						 * .field(ShiftConstants.toggleField("Complete?:"))
						 * .select("No")
						 * .field(ShiftConstants.toggleField("Complete?:"))
						 * .verify(ShiftVerifications.isNotSelected())
						 */
						// Should be able to verify if the prodesure step (row) is required or not
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(0)
						.verify(ShiftVerifications.isRequired())
						// Should be able to go back to the List after verification of the row
						/**
						 * .field(ShiftConstants.toggleField("Hide optional steps?:"))
						 * .select("No")
						 */

						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)

						.record(1)
						.verify(ShiftVerifications.isNotRequired())

						.action(ShiftActionTypes.actionByLabel("Done", ShiftActionTypes.SHIFT_APP))
						.click()

						// To complete an incomplete required procedure step
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.search("Asset")
						.row(0)
						.click()
						/**
						 * .field(ShiftConstants.toggleField("Hide optional steps?:"))
						 * .select("Yes")
						 */
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.filterBy("Incomplete")
						// save the count
						.saveCountTo(count)
						// need to be in a method:if the count is not zero, then go to the first row record, mark it complete
						.record(0)
						// .field(ShiftConstants.toggleField("Complete?:"))
						// .verify(ShiftVerifications.isDisabled())
						.field(ShiftConstants.textField("Reading"))
						.enter("2239")
						// .field(ShiftConstants.toggleField("Complete?:"))
						// .select("Yes")
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)

						// Should be able to get the progress from progress bar
						.field(ShiftConstants.progressBarField())
						.verify(ShiftVerifications.eq(totalNumberCompletedPSteps.toInt() + "\n" + " / \n"
								+ totalNumberPSteps.toInt() + "\n" + "  �  \n" + totalNumberCompletedRequiredPSteps.toInt()
								+ "\n" + " / \n" + totalNumberRequiredPSteps.toInt() + "\n" + " Required"))

						// ***********************************************************
						.$(WorkTaskUXUtilV2.UNFOLD_TIME_SECTION())
						.verify(ShiftVerifications.contains("Logged time"))
						.table("Logged time")

						.saveCountTo(count)
						.row(0)
						.action(ShiftActionTypes.actionByLabel("Edit", ShiftActionTypes.SHIFT_APP))
						.click()
						.action(ShiftActionTypes.actionByLabel("Cancel", ShiftActionTypes.SHIFT_APP))
						.click()
						.table("Logged time")
						.action(ShiftActionTypes.actionByIcon("ibm-glyphs:incremental-input-add",
								ShiftActionTypes.SHIFT_APP))
						.click()
						.action(ShiftActionTypes.actionByLabel("Cancel", ShiftActionTypes.SHIFT_APP))
						.click()

						.$(WorkTaskUXUtilV2.UNFOLD_ASSETS_SECTION())
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.verify(ShiftVerifications.contains("Exhaust Fan Motor A"))
						.appPage()

						.$(WorkTaskUXUtilV2.UNFOLD_REQUESTS_SECTION())
						.action(ShiftActionTypes.actionByLabel("Duplicate Requests", ShiftActionTypes.SHIFT_APP))
						.click()
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.verify(ShiftVerifications.contains(""))
						.verify(ShiftVerifications.recordCount(2))
						.appPage()

						// .field(ShiftConstants.accordionField("Location"))
						// .unfold()
						// .verify(ShiftVerifications.contains("To be implemented later"))

						// Verify home button should bring user back to WPS portal
						.logout();
			}
		});
	}
}