package com.ibm.tivoli.tririga.tests.qa.application.testcases.bvt.wts;

import com.ibm.tivoli.tririga.automation.framework.BaseTestv2;
import com.ibm.tivoli.tririga.automation.framework.LoginPage;
import com.ibm.tivoli.tririga.automation.framework.application.VersionControl;
import com.ibm.tivoli.tririga.automation.framework.application.person.Person;
import com.ibm.tivoli.tririga.automation.framework.application.person.PersonObjects;
import com.ibm.tivoli.tririga.automation.framework.application.utils.v2.FormUtils;
import com.ibm.tivoli.tririga.automation.framework.application.utils.v2.NavigationUtils;
import com.ibm.tivoli.tririga.automation.framework.application.utils.v2.StatusUtils;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.WORKTASK_PRIORITY;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.data.WorkTaskData;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.util.WorkTaskUXUtilV2;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.util.WorkTaskUtilV2;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.web.WorkTask;
import com.ibm.tivoli.tririga.automation.framework.services.ScriptTool;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.action.ShiftActionTypes;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.app.content.ShiftAppPageContentTypes;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.page.ShiftPageContentTypes;
import com.ibm.tivoli.tririga.automation.framework.utils.FileUtils;
import com.ibm.tivoli.tririga.automation.webdriver.framework.TririgaUxApp;
import com.ibm.tivoli.tririga.automation.webdriver.framework.shiftpolymer.ShiftConstants;
import com.ibm.tivoli.tririga.automation.webdriver.framework.shiftpolymer.SortBy;
import com.ibm.tivoli.tririga.automation.webdriver.framework.shiftpolymer.verification.ShiftVerifications;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.CommandOn;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.DateUtils;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.wrappers.AutomationString;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.wrappers.AutomationWrappers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

public class WTS_BVT_LargeScreen extends BaseTestv2 {
	protected static final String TASK_NAME = "CREATE WORKTASK";
	protected static final String TASK_NAME_UX = "CREATE WORKTASK IN UX";
	protected static final String DRAFT_TASK_NAME = "CREATE DRAFT WORKTASK IN UX";
	protected static final String TASK_LOCATION = "SL TEST Building";
	protected static final String TASK_FLOOR = "Floor 01";
	protected static final String TASK_ROOM = "B100";
	protected static final String TASK_REQUEST_CLASS = "Appliances";
	protected static final String TASK_SERVICE_CLASS = "Electrical";
	protected static final String PRIORITY = "Emergency";
	protected static final String START_TIME = "01/23/2018 18:23:42";
	protected static final String STOP_TIME = "01/23/2018 18:30:42";
	protected static final String Comment = "Posting a comment for work task- In Large screen";

	protected static final String TASK_TYPE = "Corrective";
	protected static final String TASK_LOCATION1 = "SL TEST Building";
	protected static final String USER_FULLNAME = "Service Manager1";
	protected static final String Specification_Name = "Blower";
	protected static final String Spec_ID = "BL-100";
	protected static final String Spec_Class = "Blowers";
	protected static final String Model_Number = "2312";
	protected static final String SR_ID = "1000002";
	protected static final String In_Service_Date = DateUtils.getToday();
	protected static final String Asset_Status_Assigned = "Assigned";
	protected static final String Asset_Status_Available = "Available";
	protected static final String Asset_Status_Lost = "Lost";
	protected static final String Assign_Date = DateUtils.getToday();;
	protected static final String Return_Due_Date = DateUtils.getNextYear();
	protected static final String Assigned_To = "admin1 admin1";
	protected static final String ASSET_LOCATION = "Las Vegas - Warehouse";
	protected static final String ASSET_NAME1 = "Blower coil unit 1";
	protected static final String ASSET_NAME2 = "Fan Blade 2";
	protected static final String ASSET_NAME3 = "Lubricate pumps 2";
	protected static final List<String> ASSETS = new ArrayList<>(Arrays.asList(ASSET_NAME1, ASSET_NAME2, ASSET_NAME3));
	protected static final String PROCEDURE_PREFIX = "Automation_";

	protected static final String STEP_TITLE = "Travel tip 2";
	protected static final String STEP_DESCRIPTION = "Always pack a towel.";
	protected static final String READING_VALUE = "degrees-fahrenheit";
	protected static final String READING = "22";
	protected static final String COMMENTS = "Comments through Automation";
	protected static final String Assigned_people = "Service Manager1";
	protected static final String CATEGORY = "Electrical and lighting";
	protected static final String SUB_CATEGORY = "Electrical Outlets Out";
	public final static String sScriptTestArea = "Work Task Services";
	public final static String sScriptDescription = "Hogwarts release: ";
	public final static String sScriptTCDBID = "";
	public static String sScriptTitle = "WTS Large Screen";
	private final String dataPath = FileUtils.getDirectory(this.getClass()) + "data" + "\\";
	public final String sScriptTCDBDB = "<ENTER TCDB DATABASE FILENAME HERE dirsec.nsf>";
	public String logFileName;

	/**
	 * Always specify the group
	 */
	@Test(groups = {
			"WTS_BVT_LargeScreen"
	})
	public void test_workTaskServices_bvt() {
		// BEGIN: Do not alter code within this block ***
		ScriptTool.runMainSetup(sScriptTestArea, sScriptTitle, sScriptDescription, sScriptTCDBID, this.logFileName);
		// END: Do not alter code within this block ***

		this.runFrameworkTest(new CommandOn<LoginPage>() {

			@Override
			public void executeOn(final LoginPage loginPage) {

				final AutomationString ProgressBarValue = AutomationWrappers.createString();
				final AutomationString dialogContent = AutomationWrappers.createString();
				final String filePath = WTS_BVT_LargeScreen.this.dataPath + "Comments_photo.jpg";
				final String Start = START_TIME;
				final String Stop = STOP_TIME;
				final Person WorktaskCreator = PersonObjects.WTS_managerC;

				// final AutomationInteger count = AutomationWrappers.createInteger();

				final WorkTaskData WORKTASK = new WorkTask();

				final String Id = null;

				WORKTASK.setId(Id);
				WORKTASK.setName(TASK_NAME);
				WORKTASK.setType(TASK_TYPE);
				WORKTASK.setPriority(WORKTASK_PRIORITY.HIGH);
				WORKTASK.setPrimaryLocation(TASK_LOCATION1);
				WORKTASK.setPeopleResource(WorktaskCreator.getFullName());

				loginPage
						.shiftAppLogin(WorktaskCreator.getUsername(), WorktaskCreator.getPassword(),
								TririgaUxApp.WORKTASK_SERVICES)
						.pageContent(ShiftPageContentTypes.SHIFT_DEFAULT)
						.app()
						.section("tricomp-search")
						.section("tricomp-search-type")
						.field(ShiftConstants.radioGroupField("Task"))
						.verify(ShiftVerifications.isSelected("Task"))
						.section("tricomp-search")
						.section("tricomp-search-type")
						.field(ShiftConstants.radioGroupField("Location"))
						.select("Location")
						.verify(ShiftVerifications.isSelected("Location"))
						.section("tricomp-search")
						.section("tricomp-search-type")
						.field(ShiftConstants.radioGroupField("Asset"))
						.verify(ShiftVerifications.isSelected("Asset"))
						.menuItem("Create Service Request")
						.click()
						.verify(ShiftVerifications.contains("Service Request"))
						.field(ShiftConstants.radioGroupField("Who is this request for?"))
						.verify(ShiftVerifications.isSelected("Me"))
						// // Test case 2568: WorkTaskServicesUX: ServiceRequest: Verfy that the user is able to change the location and select a Floor and room w.r.t the location/building
						.action(ShiftActionTypes.actionByIcon("ibm-glyphs:search",
								ShiftActionTypes.SHIFT_LOCATE_BUILDING_DIALOG))
						.click()
						.field(ShiftConstants.searchField())
						.enter(TASK_LOCATION1)
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.click()
						.field(ShiftConstants.locatorField("Floor"))
						.select("Floor 01")

						.field(ShiftConstants.locatorField("Problem Category"))
						.select(CATEGORY)
						.field(ShiftConstants.locatorField("Problem Type"))
						.select(SUB_CATEGORY)
						// // Test case 2569: WorkTaskServicesUX: ServiceRequest: Verify the user is able to create a submit the Service request
						.action(ShiftActionTypes.actionByLabel("Submit", ShiftActionTypes.SHIFT_DIALOG))
						.click()
						.saveValueTo(dialogContent)
						.action(ShiftActionTypes.actionByLabel("Close", ShiftActionTypes.SHIFT_APP))
						.click()
						.logout();
				final int ID = Integer.parseInt(dialogContent.toString().replaceAll("[^0-9]", ""));
				final String Request_ID = String.valueOf(ID);
				System.out.println(Request_ID);

				loginPage.loginAsDefaultUser()

						.$(NavigationUtils.MAIN_TO_ASSETS_PAGE)
						.$(WorkTaskUtilV2.OPEN_ASSET(ASSET_NAME1))
						.$(WorkTaskUtilV2.ASSIGN_ASSET(Model_Number))
						.$(NavigationUtils.MAIN_TO_ASSETS_PAGE)
						.$(WorkTaskUtilV2.OPEN_ASSET(ASSET_NAME3))
						.$(WorkTaskUtilV2.LOST_ASSET())

						// Navigate to Main Work Task Page
						.$(NavigationUtils.MAIN_TO_WORK_TASK_PAGE)

						.$(WorkTaskUtilV2.CREATE_WORKTASK(WORKTASK)) // Create Work task
						.$(WorkTaskUtilV2.OPEN_WORK_TASK(WORKTASK.getId()))
						.$(StatusUtils.WAIT_FOR_STATUS("Active", 50, 20))
						.$(FormUtils.NAVIGATE_TO_TAB("Work Details"))
						.$(WorkTaskUtilV2.ADD_BULK_PROCEDURES(PROCEDURE_PREFIX))
						.$(WorkTaskUtilV2.ADD_ASSETS(ASSETS))
						.$(WorkTaskUtilV2.ADD_REQUEST(Request_ID))

						.$(WorkTaskUtilV2.SAVE_AND_CLOSE_WORK_TASK())
						.logout();

				loginPage
						.shiftAppLogin(WorktaskCreator.getUsername(), WorktaskCreator.getPassword(),
								TririgaUxApp.WORKTASK_SERVICES)
						.pageContent(ShiftPageContentTypes.SHIFT_DEFAULT)
						.app()

						.$(WorkTaskUXUtilV2.TAB("In Progress")) // Click on In Progress tab
						.$(WorkTaskUXUtilV2.FILTER_BY("All")) // Filter by All tab

						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.sortDescending("Planned Start")

						.row(0)
						.click()
						// // Test case 946: SeleniumV2 - Automation - WTS- Verify locations field is populated when clicked on a certain worktask
						.$(WorkTaskUXUtilV2.UNFOLD_LOCATIONS_SECTION())
						.section("tricomp-task-detail-locations")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						// // Test case 947: SeleniumV2 - Automation - WTS-Verify Primary work locations shows up by default when clicked on a certain worktask Location Field.
						.verify(ShiftVerifications.contains(TASK_LOCATION1))
						.verify(ShiftVerifications.hasNoAction("ibm-glyphs:location"))
						.section("tricomp-location-map-link")
						.verify(ShiftVerifications.hasAction("ibm:booklet-guide"))// Verify the map icon has action
						.appPage()
						//
						.$(WorkTaskUXUtilV2.UNFOLD_REQUESTS_SECTION())
						.section("tricomp-task-detail-requests")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.headerRow()
						.column(2)
						.verify(ShiftVerifications.hasIcon("ibm-glyphs:phone-call"))
						.appPage()
						.section("tricomp-task-detail-requests")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						// //
						.headerRow()
						.column(3)
						.verify(ShiftVerifications.hasIcon("ibm:mobile"))
						.appPage()
						.section("tricomp-task-detail-requests")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.headerRow()
						.column(4)
						.verify(ShiftVerifications
								.hasIcon(VersionControl.isHogwartsOrLater() ? "ibm-glyphs:mail" : "ibm:email"))
						.appPage()
						.section("tricomp-task-detail-requests")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.verify(ShiftVerifications.contains("Primary request"))
						.verify(ShiftVerifications.contains(Request_ID))
						.appPage()
						//
						.$(WorkTaskUXUtilV2.UNFOLD_ASSIGNED_PEOPLE_SECTION())
						.section("tricomp-task-detail-people")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						// // Test case 982: SeleniumV2 - Automation - Verifies the user's data on fullname,picture, Mobile number, phone number and email
						.verify(ShiftVerifications.contains(Assigned_people))
						.appPage()
						// // Test case 371: SeleniumV2 - Automation - WTS- Verify and validate comments section
						// // Test case 373: SeleniumV2 - Automation - WTS- Validate photos section after uploading a photo
						// // Test case 375: SeleniumV2 - Automation - WTS- Verify comments section if input provided in "Text area", the "Post" button should be enabled.
						//
						// // Test case 457: SeleniumV2 -Automation - [WorkTask-UI] Verify the user is able to expand the "Assets" list associated to a certain worktask.
						.$(WorkTaskUXUtilV2.UNFOLD_ASSETS_SECTION())
						.section("tricomp-task-detail-assets")
						//
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						// // Test case 479: SeleniumV2 - Automation - WTS- [WorkTask-UI] - Verify when cliked on Assets field, it displays the asset names
						.verify(ShiftVerifications.contains(ASSET_NAME1))
						.verify(ShiftVerifications.contains(ASSET_NAME2))
						.verify(ShiftVerifications.contains(ASSET_NAME3))
						// // Test case 459: SeleniumV2 -Automation - [WorkTask-UI] - Verify when clicked on the "Assets" field, it displayes the location and map icons.
						.verify(ShiftVerifications.hasAction("ibm-glyphs:location"))
						.section("tricomp-location-map-link")
						.verify(ShiftVerifications.hasAction("ibm:booklet-guide"))
						.appPage()
						.section("tricomp-task-detail-assets")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						//
						.click()
						// // Test case 466: SeleniumV2 -Automation -[WorkTask-UI] - Verify when clicked on "Asset details" tab for a certain asset it should display the following under "Spec
						// // information": 1. Specification Name 2.Spec ID 3. Spec Class 4. Brand 5. Model Number
						//
						// // Test case 467: SeleniumV2 -Automation -[WorkTask-UI] - Verify when clicked on "Asset details" tab for a certain asset it should display the following under "Asset
						// // Status":
						// // // 1. In Service date 2. Asset Status 3. Assign Date 4. Return due date 5. Assigned To
						.section("tricomp-asset-specific-detail")
						.verify(ShiftVerifications.contains(Specification_Name))
						.verify(ShiftVerifications.contains(Spec_ID))
						.verify(ShiftVerifications.contains(Spec_Class))
						.verify(ShiftVerifications.contains(Model_Number))
						.verify(ShiftVerifications.contains(In_Service_Date))
						.verify(ShiftVerifications.contains(Asset_Status_Assigned))
						.verify(ShiftVerifications.contains(Assign_Date))
						.verify(ShiftVerifications.contains(Return_Due_Date))
						.verify(ShiftVerifications.contains(Assigned_To))
						//
						.appPage()
						.$(WorkTaskUXUtilV2.TAB("Floor Plan")) // Click on Floor Plan tab
						.section("tricomp-floor-plan")

						.appContent(ShiftAppPageContentTypes.FLOOR_PLAN)
						.appPage()
						.section("tricomp-asset-detail-card")
						.verify(ShiftVerifications.contains(ASSET_LOCATION)) // Verify the Asset Location
						.appPage()
						.section("tricomp-floor-plan")
						.section("triplat-graphic")
						.section("triplat-graphic-base")
						.verify(ShiftVerifications.notContains("Floor plan not available.")) // Verify it is having Floor plan
						.appPage()
						// // Test case 464: SeleniumV2 -Automation -[WorkTask-UI] - Verify that the user is able to zoom in/out the Floor plan using slider .
						.section("tricomp-floor-plan")
						.section("triplat-zoom-slider")
						.verify(ShiftVerifications.hasAction("ibm-glyphs:incremental-input-add")) // Verify the "+" button has action
						.verify(ShiftVerifications.hasAction("ibm-glyphs:incremental-input-minus"))// Verify the "-" button has action
						.appPage()
						//
						.$(WorkTaskUXUtilV2.CLICK_HOME_ICON)
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.sortDescending("Planned Start")
						// // Test case 527: SeleniumV2 - Automation - WTS - Verify that the tasks can be sorted w.r.t Priority, Location, Planned start, Status
						.verify(ShiftVerifications.isSorted(SortBy.PLANNED_START, false))
						.row(0)
						.click()
						//
						.$(WorkTaskUXUtilV2.UNFOLD_PROCEDURES_SECTION())
						// // Test case 991: SeleniumV2 - Automation - WTS- Verify if the user is able to filter the procedure by asset /Location/Tasks
						.section("tricomp-task-detail-procedures")
						.section("tricomp-procedure-list-filter")
						.filterBy("Location")
						.appPage()
						.section("tricomp-task-detail-procedures")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						//
						.verify(ShiftVerifications.recordCount(2))
						.appPage()
						//
						.section("tricomp-task-detail-procedures")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.appPage()
						// // Test case 993: SeleniumV2 - Automation - WTS- Per Task - Verify the fields in the procedure details
						//
						.$(WorkTaskUXUtilV2.OPEN_PROCEDURE("Automation_Location_Procedure"))
						// // Test case 1257: Selenium V2 - automation - WTS - Verify if the Complete action for the procedure steps is present against each procedure steps.
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(0)
						//
						.section("tricomp-procedure-step-status")
						.field(ShiftConstants.toggleField("Complete?:"))
						.verify(ShiftVerifications.isNotSelected())
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(1)
						.section("tricomp-procedure-step-status")
						.field(ShiftConstants.toggleField("Complete?:"))
						.verify(ShiftVerifications.isNotSelected())
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(1)
						//
						.verify(ShiftVerifications.contains(STEP_TITLE))
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(1)
						.verify(ShiftVerifications.contains(STEP_DESCRIPTION))
						// // Test case 1260: Selenium V2 - automation - WTS -Verify navigating to procedure details from procedure step page saves the comments and readings in procedure steps
						// // Test case 1261: Selenium V2 - automation - WTS -verify if the user is able to change the reading once completed
						//
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(1)
						.section("triplat-number-input")
						.section("paper-input")
						.verify(ShiftVerifications.contains("Reading"))
						.appPage()
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(1)
						.section("paper-textarea")
						.verify(ShiftVerifications.contains("Comments"))
						//
						.appPage()
						//
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(1)
						.verify(ShiftVerifications.contains(READING_VALUE))
						//
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(1)
						.section("tricomp-procedure-step-status")
						.field(ShiftConstants.toggleField("Complete?:"))
						.verify(ShiftVerifications.hasNoAction("Complete?:"))
						// // Test case 1256: Selenium V2 - automation - WTS -Verify if the user is able to complete the procedure step with comments
						//
						.$(WorkTaskUXUtilV2.COMPLETE_READING_PROCEDURE_STEP(1, READING, COMMENTS))
						.$(WorkTaskUXUtilV2.COMPLETE_NON_READING_PROCEDURE_STEP(0, COMMENTS))
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(0)

						.verify(ShiftVerifications.contains(USER_FULLNAME))
						//
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(1)
						.section("tricomp-procedure-step-status")
						.field(ShiftConstants.toggleField("Complete?:"))
						.verify(ShiftVerifications.isSelected())
						//
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(1)
						.section("triplat-number-input")
						.section("paper-input")
						.field(ShiftConstants.textField("Reading"))
						.verify(ShiftVerifications.isReadOnly())
						.appContent(ShiftAppPageContentTypes.PROCEDURE_STEP_LIST)
						.record(1)
						.appPage()
						.field(ShiftConstants.progressBarField())
						.saveValueTo(ProgressBarValue)
						// // Test case 994: SeleniumV2 - Automation - WTS- Verify if clicking on "done" returns the user to procedure list page
						.action(ShiftActionTypes.actionByLabel("Done", ShiftActionTypes.SHIFT_APP))
						.click()
						.$(WorkTaskUXUtilV2.POST_COMMENT(Comment, filePath))
						.$(WorkTaskUXUtilV2.CLICK_HOME_ICON)

						// Test case 412: SeleniumV2 - Automation - WTS-[WorkTask-UI] - Verify that the "Create Task" item is displayed when the user opens WTS UX portal.
						.$(WorkTaskUXUtilV2.CLICK_CREATE_TASK_MENUITEM)
						//
						.section("paper-input")
						.field(ShiftConstants.textField("Name"))
						.enter(TASK_NAME_UX)
						// // Test case 422: SeleniumV2 - Automation - WTS-[WorkTask-UI] - Verify that user is able to change the priority of the task created.
						.$(WorkTaskUXUtilV2.SET_PRIORITY("None"))
						.$(WorkTaskUXUtilV2.SET_PRIORITY(PRIORITY))
						.section("triapp-location-context")
						.section("tricomp-location-context")
						// // Test case 423: SeleniumV2 - Automation - WTS-[WorkTask-UI] - Verify that Clicking on the look up button for searching a location opens up a new pop up window for
						// // searching a
						// // // location.
						.action(ShiftActionTypes.actionByIcon("ibm-glyphs:search",
								ShiftActionTypes.SHIFT_LOCATE_BUILDING_DIALOG))
						//
						.click()
						//
						.verify(ShiftVerifications.contains("Locate a Building"))
						.searchBuilding_dialog()
						// // Test case 425: SeleniumV2 - Automation - WTS-[WorkTask-UI] - Verify that the user is able to search for the location for which the work task was created.
						.field(ShiftConstants.searchField())
						.enter(TASK_LOCATION)
						.searchBuilding_dialog()
						.section("triblock-search")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.click()
						.field(ShiftConstants.locatorField("Floor"))
						.select(TASK_FLOOR)
						// // Test case 427: SeleniumV2 - Automation - WTS-[WorkTask-UI] - Verify that the user is able to select the request class and service class of the work task.
						.field(ShiftConstants.locatorField("Request Class"))
						.select(TASK_REQUEST_CLASS)
						.field(ShiftConstants.locatorField("Service Class"))
						.select(TASK_SERVICE_CLASS)
						.field(ShiftConstants.textAreaField("Problem Description"))
						.enter("Create Work Task in UX app")
						.section("triapp-comments")
						.field(ShiftConstants.textAreaField("Comments and Photos"))
						.enter("Create Work Task in UX app")
						.section("triapp-comments")
						//
						.action(ShiftActionTypes.actionByLabel("Post", ShiftActionTypes.SHIFT_APP))
						.click()
						.section("tricomp-task-detail-actions")
						// // Test case 431: SeleniumV2 - Automation - WTS-[WorkTask-UI] - Verify that the "Delete" and "submit" button is on the top right of the new task page.
						.verify(ShiftVerifications.contains("Delete"))
						// // Test case 430: SeleniumV2 - Automation - WTS-[WorkTask-UI] - Verify that the task is getting created when clicked on "Submit" button.
						.action(ShiftActionTypes.actionByLabel("Submit", ShiftActionTypes.SHIFT_APP))
						.click()
						.$(WorkTaskUXUtilV2.TAB("In Progress"))
						.$(WorkTaskUXUtilV2.TAB("Draft"))
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.sortDescending("Planned Start")
						.verify(ShiftVerifications.isSorted(SortBy.PLANNED_START, false))
						.row(0)
						.verify(ShiftVerifications.notContains("Review In Progress"))
						.appPage()

						.$(WorkTaskUXUtilV2.TAB("In Progress"))
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.sortDescending("Planned Start")
						.verify(ShiftVerifications.isSorted(SortBy.PLANNED_START, false))
						.row(0)
						.click()
						//
						.verify(ShiftVerifications.contains(TASK_NAME_UX))
						.verify(ShiftVerifications.contains(TASK_LOCATION))
						.verify(ShiftVerifications.contains(TASK_FLOOR))
						//
						.verify(ShiftVerifications.contains(TASK_REQUEST_CLASS))
						.verify(ShiftVerifications.contains(TASK_SERVICE_CLASS))
						.verify(ShiftVerifications.contains(PRIORITY))
						//
						.$(WorkTaskUXUtilV2.UNFOLD_TIME_SECTION())
						// // Test case 387: SeleniumV2 - Automation - WTS-[WorkTask-UI] - Validate "Start timer" / "Stoptimer" icon when user clicked on it.
						.$(WorkTaskUXUtilV2.TIMER_ICON_ACTIONS())
						//
						// // Test case 984: SeleniumV2 - Automation - WTS- Verify if the worktask status is changed from Active to "Hold per Requester"
						//
						.$(WorkTaskUXUtilV2.HOLD_RESUME_WORKTASK_LARGE_SCREEN("Hold per Requester"))
						// // Test case 985: SeleniumV2 - Automation - WTS- Verify if the work task status is changed from "Hold per Requester" to Active
						.$(WorkTaskUXUtilV2.HOLD_RESUME_WORKTASK_LARGE_SCREEN("Resume"))
						// // Test case 959: SeleniumV2 - Automation - WTS-Verify__Complete_Task
						.$(WorkTaskUXUtilV2.COMPLETE_WORK_TASK("Completed through automation"))
						.$(WorkTaskUXUtilV2.CLICK_HOME_ICON)
						.$(WorkTaskUXUtilV2.TAB("Completed"))
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.sortDescending("Planned Start")
						.verify(ShiftVerifications.isSorted(SortBy.PLANNED_START, false))
						.row(0)
						.click()
						// // Test case 957: SeleniumV2 - Automation - WTS-Verify__Re_OpenTask
						.section("tricomp-task-detail-actions")
						.action(ShiftActionTypes.actionByLabel("Reopen", ShiftActionTypes.SHIFT_APP))
						.click()
						.section("tricomp-task-detail-actions")
						.section("triblock-popup#reopenPopup", false)
						.field(ShiftConstants.locatorField("Reason"))
						.select("Callback")
						.section("tricomp-task-detail-actions")
						.section("triblock-popup#reopenPopup", false)
						.action(ShiftActionTypes.actionByLabel("Reopen", ShiftActionTypes.SHIFT_APP))
						.click()
						.$(WorkTaskUXUtilV2.CLICK_HOME_ICON)
						// // Test case 990: SeleniumV2 - Automation - WTS- Verify if the worktask status is changed from Active -> Complete ->Close
						//
						.$(WorkTaskUXUtilV2.TAB("In Progress"))
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.sortDescending("Planned Start")
						.verify(ShiftVerifications.isSorted(SortBy.PLANNED_START, false))
						.row(0)
						.click()
						.$(WorkTaskUXUtilV2.COMPLETE_WORK_TASK("Completed through automation"))
						.$(WorkTaskUXUtilV2.CLICK_HOME_ICON)
						.$(WorkTaskUXUtilV2.TAB("Completed"))
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.sortDescending("Planned Start")
						.verify(ShiftVerifications.isSorted(SortBy.PLANNED_START, false))
						.row(0)
						.click()
						.section("tricomp-task-detail-actions")
						.action(ShiftActionTypes.actionByLabel("Close", ShiftActionTypes.SHIFT_APP))
						.click()
						.verify(ShiftVerifications.contains("Closed"))
						.$(WorkTaskUXUtilV2.CLICK_HOME_ICON)
						// Test case 2547: WorkTaskServicesUX: LocateUX: Verify that the user is able to launch the Locate app from WTS UX portal.
						.menuItem("Locate")
						.click()
						// Test case 2548: WorkTaskServicesUX: Verify that the user is able to click on Person tab in the side navigation.
						.sideNav()
						.select("Person")
						.section("tricomp-search-person")
						.section("triapp-people-search")
						// Test case 2549: WorkTaskServicesUX: Locate: Verify user is able to search for a certain person and verify the person details.
						.field(ShiftConstants.searchField())
						.enter("Locate User1")
						.section("tricomp-search-person")
						.section("triapp-people-search")
						.section("triblock-search")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.click()
						.section("tricomp-person-details")
						.verify(ShiftVerifications.contains("Locate User1"))
						.verify(ShiftVerifications.hasIcon("ibm-glyphs:phone-call"))
						.verify(ShiftVerifications.hasIcon("ibm:mobile"))
						.verify(ShiftVerifications.hasIcon("ibm:email"))
						.section("triblock-image-info-card")
						.section("triplat-image")
						.verify(ShiftVerifications.hasImage())
						.appPage()
						.section("tricomp-location-details-person")
						.section("triblock-tabs")
						// Test case 2550: WorkTaskServicesUX: Locate: Verify that once the user clicks on the Search result, 3 tabs should be displayed- Locations, Key Rooms, Floor Directory.
						.verify(ShiftVerifications.hasTab("Location", "Key Rooms", "Floor Directory"))
						// Test case 2551: WorkTaskServicesUX: Locate: Verify that when clicked on Key rooms tab, it has a floor plan and maximize/minimize buttons.
						.tab("Location")
						.click()
						.section("tricomp-location-details-person")
						.section("tripage-location")
						.section("triplat-graphic")
						.section("triplat-graphic-base")
						.verify(ShiftVerifications.notContains("Floor plan not available."))
						.appPage()
						.section("tricomp-location-details-person")

						.section("tripage-location")
						.section("triplat-zoom-slider")
						.verify(ShiftVerifications.hasAction("ibm-glyphs:incremental-input-add")) // Verify the "+" button has action
						.verify(ShiftVerifications.hasAction("ibm-glyphs:incremental-input-minus")) // Verify the "+" button has action
						.appPage()
						.section("tricomp-location-details-person")
						.section("triblock-tabs")
						// Test case 2552: WorkTaskServicesUX: Locate: Verify that clicking on Key Rooms tab, it should display a floor plan and should have a max(+) and min(-) button.
						.tab("Key Rooms")
						.click()
						.section("tricomp-location-details-person")
						.section("tripage-key-rooms")
						.section("triplat-graphic-legend")
						.verify(ShiftVerifications.hasIcon("ibm-glyphs:back"))
						.appPage()
						.section("tricomp-location-details-person")

						.section("tripage-key-rooms")
						.section("triplat-zoom-slider")
						.verify(ShiftVerifications.hasAction("ibm-glyphs:incremental-input-add")) // Verify the "+" button has action
						.verify(ShiftVerifications.hasAction("ibm-glyphs:incremental-input-minus"))
						.appPage()
						.section("tricomp-location-details-person")
						.section("triblock-tabs")
						// Test case 2553: WorkTaskServicesUX: Locate: Verify that when the user clicks on Floor directory tab, the searched person is displayed.
						.tab("Floor Directory")
						.click()
						.section("tricomp-location-details-person")
						.section("tripage-floor-directory")

						.verify(ShiftVerifications.contains("Locate User1"))

						.appPage()
						// Test case 2554: WorktaskServicesUX: Locate: Verify that the user is able to change the location and provide a Room and Floor number.
						.sideNav()
						.select("Room")
						// Select the location to locate room.
						.section("tricomp-search-room")
						.section("triapp-location-context")
						.section("tricomp-location-context")
						.action(ShiftActionTypes.actionByLabel("Change location", ShiftActionTypes.SHIFT_APP))
						.click()
						.section("tricomp-search-room")
						.section("triapp-location-context")
						.section("tricomp-location-context")
						.section("triblock-search-popup")

						.field(ShiftConstants.searchField())

						.enter("Las Vegas - Warehouse")
						.section("tricomp-search-room")
						.section("triapp-location-context")
						.section("tricomp-location-context")
						.section("triblock-search-popup")
						.section("triblock-search")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.click()

						.section("tricomp-search-room")
						.section("triblock-search#floorSearchField")
						// Select the Floor and room associated with the Location.
						.action(ShiftActionTypes.actionByIcon("ibm-glyphs:expand-close", ShiftActionTypes.SHIFT_APP))
						.click()
						.section("tricomp-search-room")
						.section("triblock-search")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row("Floor 01")
						.click()

						.section("tricomp-search-room")
						.section("triblock-search#roomSearchField")
						.field(ShiftConstants.textField(""))
						.enter("Warehouse Floor")
						.section("tricomp-search-room")
						.section("triblock-search#roomSearchField")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.click()
						// Test case 2555: WorkTaskServicesUX: Locate: Verify that after selecting a location, floor and room, it displays 4 tabs- Location, Key rooms, Floor directory, BIM
						.section("tricomp-location-details-room")
						.section("triblock-tabs")

						.verify(ShiftVerifications.hasTab("Location", "Key Rooms", "Floor Directory", "BIM"))
						// Test case 2556: WorkTaskServicesUX: Locate: Verify that under Location and key room tab, floor plan and max(+), min(-) buttons are displayed.
						.tab("Location")
						.click()
						.section("tricomp-location-details-room")
						.section("tripage-location")
						.section("triplat-graphic")
						.section("triplat-graphic-base")

						.verify(ShiftVerifications.notContains("Floor plan not available."))
						.appPage()
						.section("tricomp-location-details-room")
						.section("tripage-location")
						.section("triplat-zoom-slider")
						.verify(ShiftVerifications.hasAction("ibm-glyphs:incremental-input-add")) // Verify the "+" button has action
						.verify(ShiftVerifications.hasAction("ibm-glyphs:incremental-input-minus")) // Verify the "+" button has action
						.appPage()

						// Navigate to key room tab
						.section("tricomp-location-details-room")
						.section("triblock-tabs")
						.tab("Key Rooms")
						.click()
						.section("tricomp-location-details-room")
						.section("tripage-key-rooms")

						.section("triplat-zoom-slider")
						.verify(ShiftVerifications.hasAction("ibm-glyphs:incremental-input-add")) // Verify the "+" button has action
						.verify(ShiftVerifications.hasAction("ibm-glyphs:incremental-input-minus"))
						.appPage()

						.$(WorkTaskUXUtilV2.CLICK_HOME_ICON)
						.logout();

			}
		});
	}
}