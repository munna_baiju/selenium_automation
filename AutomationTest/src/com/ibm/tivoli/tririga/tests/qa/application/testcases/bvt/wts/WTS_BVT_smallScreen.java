package com.ibm.tivoli.tririga.tests.qa.application.testcases.bvt.wts;

import com.ibm.tivoli.tririga.automation.framework.BaseTestv2;
import com.ibm.tivoli.tririga.automation.framework.LoginPage;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.util.WorkTaskUXUtilV2;
import com.ibm.tivoli.tririga.automation.framework.services.ScriptTool;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.action.ShiftActionTypes;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.app.content.ShiftAppPageContentTypes;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.page.ShiftPageContentTypes;
import com.ibm.tivoli.tririga.automation.webdriver.framework.TririgaUxApp;
import com.ibm.tivoli.tririga.automation.webdriver.framework.shiftpolymer.verification.ShiftVerifications;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.CommandOn;

import org.testng.annotations.Test;

public class WTS_BVT_smallScreen extends BaseTestv2 {
	public final static String sScriptTestArea = "Work Task Services";
	public final static String sScriptDescription = "Phoenix release: ";
	public final static String sScriptTCDBID = "";
	public static String sScriptTitle = "Workplace Services - Locate";
	public final String sScriptTCDBDB = "<ENTER TCDB DATABASE FILENAME HERE dirsec.nsf>";
	public String logFileName;

	/**
	 * Always specify the group
	 */
	@Test(groups = {
			"WTS Small Screen"
	})
	public void test_workTaskServices_bvt() {
		// BEGIN: Do not alter code within this block ***
		ScriptTool.runMainSetup(sScriptTestArea, sScriptTitle, sScriptDescription, sScriptTCDBID, this.logFileName);
		// END: Do not alter code within this block ***

		this.runFrameworkTest(new CommandOn<LoginPage>() {

			@Override
			public void executeOn(final LoginPage loginPage) {

				loginPage.shiftAppLoginAsDefaultUser(TririgaUxApp.WORKTASK_SERVICES)
						.pageContent(ShiftPageContentTypes.SHIFT_DEFAULT)
						.app()
						.$(WorkTaskUXUtilV2.TAB("In Progress"))

						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						// defect
						// .$(WorkTaskUXUtilV2.FILTER_BY("All"))
						.verify(ShiftVerifications.contains("Planned Start"))
						.sortBy("Planned Start")
						// defect
						// .filterBy("My created tasks")
						// .$(WorkTaskUXUtilV2.FILTER_BY("All"))
						.sortBy("Planned Start")
						.record(0)
						.verify(ShiftVerifications.hasIcon("ibm:timer", "priority-medium"))
						.record(0)
						.verify(ShiftVerifications.hasAction("ibm:timer"))
						.record(0)
						.verify(ShiftVerifications.hasNoAction("priority-medium"))
						.record(0)
						.click()
						// .field(ShiftConstants.accordionField("Time"))
						// .unfold()
						// .verify(ShiftVerifications.contains("Logged time"))
						// .table("Logged time")
						// .row(0)
						// .action(ShiftActionTypes.actionByLabel("Edit", ShiftActionTypes.SHIFT_APP))
						// .click()
						// .action(ShiftActionTypes.actionByLabel("Cancel", ShiftActionTypes.SHIFT_APP))
						// .click()
						// .table("Logged time")
						// .action(ShiftActionTypes.actionByIcon("ibm-glyphs:incremental-input-add",
						// ShiftActionTypes.SHIFT_APP))
						// .click()
						// .action(ShiftActionTypes.actionByLabel("Cancel", ShiftActionTypes.SHIFT_APP))
						// .click()
						.$(WorkTaskUXUtilV2.UNFOLD_ASSETS_SECTION())
						.section("tricomp-task-detail-assets")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.verify(ShiftVerifications.contains("Exhaust Fan Motor A"))
						.appPage()
						.action(ShiftActionTypes.actionByIcon("ibm-glyphs:back", ShiftActionTypes.SHIFT_APP))
						.click()

						// .field(ShiftConstants.accordionField("Location"))
						// .unfold()
						// .verify(ShiftVerifications.contains("To be implemented later"))

						// Verify home button should bring user back to WPS portal
						.logout();
			}
		});
	}
}