package com.ibm.tivoli.tririga.tests.qa.application.testcases.bvt.wts;

import com.ibm.tivoli.tririga.automation.framework.BaseTestv2;
import com.ibm.tivoli.tririga.automation.framework.LoginPage;
import com.ibm.tivoli.tririga.automation.framework.application.VersionControl;
import com.ibm.tivoli.tririga.automation.framework.application.person.Person;
import com.ibm.tivoli.tririga.automation.framework.application.person.PersonObjects;
import com.ibm.tivoli.tririga.automation.framework.application.utils.v2.NavigationUtils;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.WORKTASK_PRIORITY;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.data.WorkTaskData;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.util.WorkTaskUXUtilV2;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.util.WorkTaskUtilV2;
import com.ibm.tivoli.tririga.automation.framework.application.worktask.web.WorkTask;
import com.ibm.tivoli.tririga.automation.framework.services.ScriptTool;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.action.ShiftActionTypes;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.app.content.ShiftAppPageContentTypes;
import com.ibm.tivoli.tririga.automation.framework.shiftpolymer.page.ShiftPageContentTypes;
import com.ibm.tivoli.tririga.automation.framework.utils.FileUtils;
import com.ibm.tivoli.tririga.automation.webdriver.framework.TririgaUxApp;
import com.ibm.tivoli.tririga.automation.webdriver.framework.shiftpolymer.ShiftConstants;
import com.ibm.tivoli.tririga.automation.webdriver.framework.shiftpolymer.verification.ShiftVerifications;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.CommandOn;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.DateUtils;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.wrappers.AutomationInteger;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.wrappers.AutomationString;
import com.ibm.tivoli.tririga.automation.webdriver.framework.util.wrappers.AutomationWrappers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

public class WTS_smallscreen_BVT extends BaseTestv2 {
	public final static String sScriptTestArea = "Work Task Services";
	public final static String sScriptDescription = "Phoenix release: ";
	public final static String sScriptTCDBID = "";
	protected static final String TASK_NAME = "WTS-smallScreen-BVT";
	protected static final String TASK_TYPE = "Corrective";
	protected static final String TASK_LOCATION = "SL TEST Building";
	protected static final String TASK_ROOM = "Mock-Rec37-Myspace";
	protected static final String TASK_REQUEST_CLASS = "Appliances";
	protected static final String TASK_SERVICE_CLASS = "Electrical";
	protected static final String PRIORITY = "Emergency";
	protected static final String TASK_FLOOR = "Floor 01";
	protected static final String USER_FULLNAME = "Service Manager2";
	protected static final String Specification_Name = "Blower";
	protected static final String Spec_ID = "BL-100";
	protected static final String Spec_Class = "Blowers";
	protected static final String Model_Number = "Blower coil unit 1";
	protected static final String In_Service_Date = DateUtils.getToday();
	protected static final String Asset_Status_Assigned = "Assigned";
	protected static final String Asset_Status_Available = "Available";
	protected static final String Asset_Status_Lost = "Lost";
	protected static final String Assign_Date = DateUtils.getToday();;
	protected static final String Return_Due_Date = DateUtils.getNextYear();
	protected static final String PROCEDURE_PREFIX = "Automation_";
	protected static final String ServiceRequest_ID = "1000000";
	protected static final String Assigned_To = "admin1 admin1";
	protected static final String ASSET_LOCATION = "WBDG Med Center-HYD";
	protected static final String ASSET_NAME1 = "Blower coil unit 1";
	protected static final String ASSET_NAME2 = "Fan Blade 2";
	protected static final String Comment = "Posting a comment for work task- In Small screen";
	protected static final String CATEGORY = "Computers";
	protected static final String TYPE = "Software";
	protected static final String START_TIME = "01/23/2018 18:23:42";
	protected static final String STOP_TIME = "01/23/2018 18:30:42";
	// protected static final String ASSET_NAME3 = "Lubricate pumps 2";
	protected static final List<String> ASSETS = new ArrayList<>(Arrays.asList(ASSET_NAME1, ASSET_NAME2));
	public static String sScriptTitle = "Workplace Services - BVT";

	private final String dataPath = FileUtils.getDirectory(this.getClass()) + "data" + "\\";
	final String filePath = WTS_smallscreen_BVT.this.dataPath + "Comments_photo.jpg";
	public final String sScriptTCDBDB = "<ENTER TCDB DATABASE FILENAME HERE dirsec.nsf>";
	public String logFileName;

	/**
	 * Always specify the group
	 */
	@Test(groups = {
			"WTS Small Screen"
	})
	public void test_workTaskServices_bvt() {
		// BEGIN: Do not alter code within this block ***
		ScriptTool.runMainSetup(sScriptTestArea, sScriptTitle, sScriptDescription, sScriptTCDBID, this.logFileName);
		// END: Do not alter code within this block ***

		this.runFrameworkTest(new CommandOn<LoginPage>() {

			@Override
			public void executeOn(final LoginPage loginPage) {
				final AutomationString taskid = AutomationWrappers.createString();
				final Person WorktaskCreator = PersonObjects.WTS_managerB;
				final WorkTaskData WORKTASK = new WorkTask();
				final AutomationString dialogContent = AutomationWrappers.createString();
				final String Id = null;
				WORKTASK.setId(Id);
				WORKTASK.setName(TASK_NAME);
				WORKTASK.setType(TASK_TYPE);
				WORKTASK.setPriority(WORKTASK_PRIORITY.LOW);
				WORKTASK.setPrimaryLocation(TASK_LOCATION);
				WORKTASK.setPeopleResource(USER_FULLNAME);
				final String Start = START_TIME;
				final String Stop = STOP_TIME;
				final String USER_TIMEZONE = "Asia/Calcutta";
				final AutomationInteger count = AutomationWrappers.createInteger();

				loginPage
						.shiftAppLogin(WorktaskCreator.getUsername(), WorktaskCreator.getPassword(),
								TririgaUxApp.WORKTASK_SERVICES)
						.pageContent(ShiftPageContentTypes.SHIFT_DEFAULT)
						.app()
						.header()
						.verify(ShiftVerifications.contains("Service Request"))
						// Verify that the user is able to click on Service request menu item
						.menuItem("Service Request")
						.click()

						.verify(ShiftVerifications.hasIcon("ibm-glyphs:search"))
						.action(ShiftActionTypes.actionByIcon("ibm-glyphs:search",
								ShiftActionTypes.SHIFT_LOCATE_BUILDING_DIALOG))
						.click()
						.verify(ShiftVerifications.contains("Locate a Building"))
						.searchBuilding_dialog()
						// Verify that the user is able to search for a location and select it
						.field(ShiftConstants.searchField())
						.enter(TASK_LOCATION)
						.searchBuilding_dialog()
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.click()
						// Verify that the user is able to select the Floor
						.field(ShiftConstants.locatorField("Floor"))
						.select(TASK_FLOOR)
						// Verify that the user is able to select the room
						.field(ShiftConstants.locatorField("Room"))
						.select(TASK_ROOM)
						// Verify that the user is able to select the Category
						.field(ShiftConstants.locatorField("Problem Category"))
						.select(CATEGORY)
						// Verify that the user is able to select the Problem type
						.field(ShiftConstants.locatorField("Problem Type"))
						.select(TYPE)
						// Verify the user is able to submit the request
						.action(ShiftActionTypes.actionByLabel("Submit", ShiftActionTypes.SHIFT_APP))
						.click()
						.action(ShiftActionTypes.actionByLabel("Close", ShiftActionTypes.SHIFT_APP))
						.click()
						.logout();

				final int ID = Integer.parseInt(dialogContent.toString().replaceAll("[^0-9]", ""));
				final String Request_ID = String.valueOf(ID);
				System.out.println(Request_ID);
				loginPage.loginAsDefaultUser()
						.$(NavigationUtils.MAIN_TO_ASSETS_PAGE)
						.$(WorkTaskUtilV2.OPEN_ASSET(ASSET_NAME1))
						.$(WorkTaskUtilV2.ASSIGN_ASSET(ASSET_NAME1))

						.$(NavigationUtils.MAIN_TO_WORK_TASK_PAGE)
						.$(WorkTaskUtilV2.CREATE_WORKTASK(WORKTASK))

						.$(WorkTaskUtilV2.OPEN_WORK_TASK(WORKTASK.getId()))
						.$(WorkTaskUtilV2.ADD_ASSETS(ASSETS))
						.$(WorkTaskUtilV2.ADD_REQUEST(Request_ID))
						.$(WorkTaskUtilV2.ADD_BULK_PROCEDURES(PROCEDURE_PREFIX))
						.$(WorkTaskUtilV2.SAVE_AND_CLOSE_WORK_TASK())

						// .mainPage()
						.logout();

				loginPage
						.shiftAppLogin(WorktaskCreator.getUsername(), WorktaskCreator.getPassword(),
								TririgaUxApp.WORKTASK_SERVICES)
						.pageContent(ShiftPageContentTypes.SHIFT_DEFAULT)
						.app()

						.$(WorkTaskUXUtilV2.TAB("In Progress"))
						.$(WorkTaskUXUtilV2.OPEN_WORK_TASK(WORKTASK.getId(), true))
						.verify(ShiftVerifications.contains(TASK_NAME))
						.verify(ShiftVerifications.contains(TASK_TYPE))
						// Test case 1182: Selenium V2 - Automation - WTSVerify if Requests are assigned, the request field should be expandable
						.$(WorkTaskUXUtilV2.UNFOLD_REQUESTS_SECTION())
						.$(WorkTaskUXUtilV2.DETAIL_REQUESTS_ROW(0))
						// Test case 1092: Selenium V2 - automation - WTS - Verifythat the phone icon, mobile icon and email icon is clickable

						.verify(ShiftVerifications.contains("Primary request"))
						.verify(ShiftVerifications.contains(Request_ID))
						.verify(ShiftVerifications.hasIcon("ibm-glyphs:phone-call"))
						.verify(ShiftVerifications.hasIcon("ibm:mobile"))
						.verify(ShiftVerifications
								.hasIcon(VersionControl.isHogwartsOrLater() ? "ibm-glyphs:mail" : "ibm:email"))
						.verify(ShiftVerifications.hasAction("ibm:mobile"))
						.verify(ShiftVerifications.hasAction("ibm-glyphs:phone-call"))
						.verify(ShiftVerifications
								.hasAction(VersionControl.isHogwartsOrLater() ? "ibm-glyphs:mail" : "ibm:email"))
						.appPage()

						.$(WorkTaskUXUtilV2.NAVIGATE_TO_HOME())
						.$(WorkTaskUXUtilV2.OPEN_WORK_TASK(WORKTASK.getId(), true))
						// Test case 1096: Selenium V2 - automation - WTS - Verify Assets field is populated when clicked on a certain worktask
						.$(WorkTaskUXUtilV2.UNFOLD_ASSETS_SECTION())
						.section("tricomp-task-detail-assets")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						// Test case 1098: Selenium V2 - automation - WTS - Verify Assets names when cliked on asset field inside a work task
						.verify(ShiftVerifications.contains(ASSET_NAME1))
						.verify(ShiftVerifications.contains(ASSET_NAME2))
						.verify(ShiftVerifications.contains(ASSET_LOCATION))
						// Test case 1101: Selenium V2 - automation - WTS - Verify that the asset list should not have any column headers.
						.verify(ShiftVerifications.notContains("ID"))
						.verify(ShiftVerifications.notContains("Name"))
						.verify(ShiftVerifications.notContains("Description"))
						.verify(ShiftVerifications.notContains("Building"))
						.verify(ShiftVerifications.notContains("Floor"))
						.appPage()
						// Test case 1102: Selenium V2 - automation - WTS - Verify that the certain asset has Location pin and map icon
						.$(WorkTaskUXUtilV2.DETAIL_ASSETS_ROW(0))
						.verify(ShiftVerifications.hasAction("ibm-glyphs:location"))
						.appPage()
						.$(WorkTaskUXUtilV2.DETAIL_ASSETS_ROW(0))
						.section("tricomp-location-map-link")
						.verify(ShiftVerifications.hasAction("ibm:booklet-guide"))
						.appPage()
						.$(WorkTaskUXUtilV2.DETAIL_ASSETS_ROW(1))
						.verify(ShiftVerifications.hasNoAction("ibm-glyphs:location"))
						.appPage()
						.$(WorkTaskUXUtilV2.DETAIL_ASSETS_ROW(1))
						.section("tricomp-location-map-link")
						.verify(ShiftVerifications.hasNoAction("ibm:booklet-guide"))
						.appPage()
						.section("tricomp-task-detail-assets")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						// Test case 1097: Selenium V2 - automation - WTS - Verify Assets field opens up a new asset page when clicked on inside the details page of a work task
						.row(ASSET_NAME1)
						.click()
						// Test case 1175: Selenium V2 - automation - WTS -Verify when clicked on a certain asset under"Assets" field associated to a worktask, it should contain two tabs as below:
						// 1.
						// // Floor plan 2. Asset details
						.section("triblock-tabs")
						.verify(ShiftVerifications.hasTab("Asset Details"))
						.appPage()
						// Test case 1174: Selenium V2 - automation - WTS -Verify when clicked on "Asset details" tab for a certain asset it should display the following under "Asset Status": 1. In
						// Service date 2.Asset Status 3. Assign Date 4. Return due date 5. Assigned To
						// Test case 1173: Selenium V2 - automation - WTS -Verifywhen clicked on "Asset details" tab for acertain asset it should display the followingunder "Spec information": 1.
						// SpecificationName 2.Spec ID 3. Spec Class 4. Brand 5.Model Number
						.$(WorkTaskUXUtilV2.ASSET_DETAILS_LABEL())

						.section("tricomp-asset-specific-detail")
						.verify(ShiftVerifications.contains(Specification_Name))
						.verify(ShiftVerifications.contains(Spec_ID))
						.verify(ShiftVerifications.contains(Spec_Class))
						.verify(ShiftVerifications.contains(Model_Number))
						.verify(ShiftVerifications.contains(In_Service_Date))
						.verify(ShiftVerifications.contains(Asset_Status_Assigned))
						.verify(ShiftVerifications.contains(Assign_Date))
						.verify(ShiftVerifications.contains(Return_Due_Date))
						.verify(ShiftVerifications.contains(Assigned_To))
						.appPage()
						.section("tricomp-asset-detail-card")
						.section("triblock-image-info-card")
						.section("triplat-image")
						// Test case 1179: Selenium V2 - automation - WTS -Verify the picture is available for a certain asset associated to the worktask
						.verify(ShiftVerifications.hasImage())
						.appPage()
						.$(WorkTaskUXUtilV2.TAB("Floor Plan"))
						.section("tricomp-floor-plan")
						.section("triplat-graphic")
						.section("triplat-graphic-base")
						.verify(ShiftVerifications.notContains("Floor plan not available."))
						.appPage()

						.$(WorkTaskUXUtilV2.NAVIGATE_TO_HOME())
						.$(WorkTaskUXUtilV2.TAB("In Progress"))
						.$(WorkTaskUXUtilV2.OPEN_WORK_TASK(WORKTASK.getId(), true))
						// Test case 1050: Selenium V2 - automation - WTS - Verify locations page opens up a new locations page when clicked on drop down icon of a work task

						.$(WorkTaskUXUtilV2.UNFOLD_LOCATIONS_SECTION())
						.verify(ShiftVerifications.contains(TASK_LOCATION))

						.$(WorkTaskUXUtilV2.NAVIGATE_TO_HOME())
						.$(WorkTaskUXUtilV2.TAB("In Progress"))
						.$(WorkTaskUXUtilV2.OPEN_WORK_TASK(WORKTASK.getId(), true))
						.$(WorkTaskUXUtilV2.UNFOLD_PROCEDURES_SECTION())
						// Test case 1165: Selenium V2 - automation - WTS -Verify if the user is able to filter the procedure by asset /Location/Tasks

						.section("tricomp-task-detail-procedures")
						.section("tricomp-procedure-list-filter")
						.filterBy("Location")
						.appPage()
						.section("tricomp-task-detail-procedures")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.verify(ShiftVerifications.recordCount(2))
						.appPage()
						.section("tricomp-task-detail-procedures")
						.section("tricomp-procedure-list-filter")
						.filterBy("All")
						.appPage()
						.section("tricomp-task-detail-procedures")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.verify(ShiftVerifications.recordCount(7))
						.appPage()
						// Test case 1070: Selenium V2 - automation - WTS - Verify using the search bar
						.$(WorkTaskUXUtilV2.VERIFY_PROCEDURE_FILTER_COUNT("Task", 1))
						// Test case 1071: Selenium V2 - automation - WTS - Verify if clicking on any of the procedures under procedure section in work task page opens procedure details page
						.$(WorkTaskUXUtilV2.OPEN_PROCEDURE("Task"))
						// Test case 1072: Selenium V2 - automation - WTS - Verify if the user is able to hide the optional steps
						.$(WorkTaskUXUtilV2.HIDE_OPTIONAL_STEPS())
						.$(WorkTaskUXUtilV2.COMPLETE_NON_READING_PROCEDURE_STEP(0, ""))
						// Test case 1073: Selenium V2 - automation - WTS - Verify if clicking on "done" returns the user to procedure list page
						.action(ShiftActionTypes.actionByLabel("Done", ShiftActionTypes.SHIFT_APP))
						.click()

						.$(WorkTaskUXUtilV2.NAVIGATE_TO_HOME())
						.$(WorkTaskUXUtilV2.TAB("In Progress"))
						.$(WorkTaskUXUtilV2.OPEN_WORK_TASK(WORKTASK.getId(), true))
						.$(WorkTaskUXUtilV2.UNFOLD_ASSIGNED_PEOPLE_SECTION())
						// Test case 1062: Selenium V2 - automation - WTS - Verifies the Username,labour detail, Phone Icon, Mobile and email Icon
						.section("tricomp-task-detail-people")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(USER_FULLNAME)
						.verify(ShiftVerifications.contains(USER_FULLNAME))
						.appPage()
						.section("tricomp-task-detail-people")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(USER_FULLNAME)
						.verify(ShiftVerifications.hasIcon("ibm-glyphs:phone-call"))
						.record(USER_FULLNAME)
						.verify(ShiftVerifications.hasIcon("ibm:mobile"))
						.record(USER_FULLNAME)
						.verify(ShiftVerifications
								.hasIcon(VersionControl.isHogwartsOrLater() ? "ibm-glyphs:mail" : "ibm:email"))
						.appPage()

						.$(WorkTaskUXUtilV2.NAVIGATE_TO_HOME())
						.$(WorkTaskUXUtilV2.TAB("In Progress"))
						.$(WorkTaskUXUtilV2.OPEN_WORK_TASK(WORKTASK.getId(), true))

						.$(WorkTaskUXUtilV2.POST_COMMENT(Comment, WTS_smallscreen_BVT.this.filePath))

						.$(WorkTaskUXUtilV2.TAB("In Progress"))
						.$(WorkTaskUXUtilV2.OPEN_WORK_TASK(WORKTASK.getId(), true))
						// Test case 1051: Selenium V2 - automation - WTS - Verify if the Down arrow against the actual end displays the time log of the users
						.$(WorkTaskUXUtilV2.UNFOLD_TIME_SECTION())
						.section("tricomp-task-detail-time")
						.verify(ShiftVerifications.contains("There are no time logs."))
						.appPage()
						.header()
						.action(ShiftActionTypes.actionByIcon("ibm-glyphs:back", ShiftActionTypes.SHIFT_APP))
						.click()
						.$(WorkTaskUXUtilV2.TIMER_ICON_ACTIONS())
						.$(WorkTaskUXUtilV2.UNFOLD_TIME_SECTION())
						// Test case 1053: Selenium V2 - automation - WTS - Add a new time entry
						.$(WorkTaskUXUtilV2.ADD_NEW_TIME_ENTRY(Start, Stop))
						.$(WorkTaskUXUtilV2.NAVIGATE_TO_HOME())
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(WORKTASK.getId())
						.click()
						.$(WorkTaskUXUtilV2.UNFOLD_TIME_SECTION())
						// Test case 1054: Selenium V2 - automation - WTS - Remove the time entry recorded
						.$(WorkTaskUXUtilV2.DELETE_TIME_ENTRY())
						// Test case 1055: Selenium V2 - automation - WTS - Verify if "AddRow" Hyperlink is present under timelog page
						.section("tricomp-task-detail-time")
						// Test case 1057: Selenium V2 - automation - WTS - Verify if new page is opened when Add Row is clicked
						.verify(ShiftVerifications.hasAction("Add"))
						.action(ShiftActionTypes.actionByLabel("Add", ShiftActionTypes.SHIFT_TIMELOG_DIALOG))
						.click()
						// Test case 1183: Selenium V2 - automation - WTS - Verify if the new time entry has the current date and time
						.$(WorkTaskUXUtilV2.TIMELOG_FIELD("triplat-datetime-picker#plannedStart"))
						.verify(ShiftVerifications.contains(DateUtils.getTimeStampDefaultValue(USER_TIMEZONE)))
						.appPage()
						.timeLog_dialog()
						.$(WorkTaskUXUtilV2.TIMELOG_FIELD("triplat-datetime-picker#plannedEnd"))
						.verify(ShiftVerifications.notEq(""))
						.appPage()
						.timeLog_dialog()
						.action(ShiftActionTypes.actionByLabel("Cancel", ShiftActionTypes.SHIFT_APP))
						.click()
						// Test case 1058: Selenium V2 - automation - WTS - Verify if the newly added time is present in the timelog
						.section("tricomp-task-detail-time")
						.section("tricomp-time-table")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.saveCountTo(count)
						.appPage()
						.header()
						.action(ShiftActionTypes.actionByIcon("ibm-glyphs:back", ShiftActionTypes.SHIFT_APP))
						.click()
						// Verify if clicking cancel moves back to the worktask page
						.$(WorkTaskUXUtilV2.UNFOLD_TIME_SECTION())
						.section("tricomp-task-detail-time")
						.section("tricomp-time-table")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.click()
						.timeLog_dialog()
						.action(ShiftActionTypes.actionByLabel("Cancel", ShiftActionTypes.SHIFT_APP))
						.click()
						.verify(ShiftVerifications.contains(WORKTASK.getId()))
						.section("tricomp-task-detail-time")
						.section("tricomp-time-table")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.click()
						.timeLog_dialog()
						// Test case 1061: Selenium V2 - automation - WTS - Verify if the "Edit" Hyperlink re-directs the user to a new page to edit the time log
						.$(WorkTaskUXUtilV2.VERIFY_TIME_ENTRY_FIELDS())
						.appPage()
						.timeLog_dialog()
						.action(ShiftActionTypes.actionByLabel("Cancel", ShiftActionTypes.SHIFT_APP))
						.click()

						.$(WorkTaskUXUtilV2.NAVIGATE_TO_HOME())

						.header()
						.menuItem("Create Task")
						.click()
						// Verify that the new work task page contains a Name field
						.section("paper-input")
						.verify(ShiftVerifications.contains("Name"))
						// Verify that the user is able to enter a task name
						.field(ShiftConstants.textField("Name"))
						.enter(TASK_NAME)

						.section("triapp-location-context")

						.section("tricomp-location-context")

						.verify(ShiftVerifications.hasIcon("ibm-glyphs:search"))
						.action(ShiftActionTypes.actionByIcon("ibm-glyphs:search",
								ShiftActionTypes.SHIFT_LOCATE_BUILDING_DIALOG))
						.click()
						.verify(ShiftVerifications.contains("Locate a Building"))
						.searchBuilding_dialog()
						// Verify the user is able to search and select for a location.
						.field(ShiftConstants.searchField())
						.enter(TASK_LOCATION)
						.searchBuilding_dialog()
						.section("triblock-search")
						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.click()
						// Verify that the user is able to select a Floor
						.field(ShiftConstants.locatorField("Floor"))
						.select(TASK_FLOOR)
						// Verify that is user is able to select a room
						.field(ShiftConstants.locatorField("Room"))
						.select(TASK_ROOM)
						// Verify that the user is able to select a Request class
						.field(ShiftConstants.locatorField("Request Class"))
						.select(TASK_REQUEST_CLASS)
						// Verify that the user is able to select a Service class
						.field(ShiftConstants.locatorField("Service Class"))
						.select(TASK_SERVICE_CLASS)
						// Verify that the user is able to Enter a comment for Description
						.field(ShiftConstants.textAreaField("Problem Description"))
						.enter("Create Work Task in UX app")
						.section("triapp-comments")
						// Verify that the user is able to comment on comment and photos section
						.field(ShiftConstants.textAreaField("Comments and Photos"))
						.enter("Create Work Task in UX app")
						.section("triapp-comments")
						// Verify that the user is able to attach a photo
						.field(ShiftConstants.iconField("Attach a photo"))
						.select(WTS_smallscreen_BVT.this.filePath)
						.section("triapp-comments")
						// Verify that the user is able to Post the Comment and photo
						.verify(ShiftVerifications.contains("Post"))
						.action(ShiftActionTypes.actionByLabel("Post", ShiftActionTypes.SHIFT_APP))
						.click()
						.section("tricomp-task-detail-actions", true, false)
						// Verify that the New task page is having Submit and Delete button
						.verify(ShiftVerifications.contains("Delete"))
						.verify(ShiftVerifications.contains("Submit"))
						.appPage()
						.section("triapp-location-context")
						.section("tricomp-location-context")
						.verify(ShiftVerifications.contains(TASK_LOCATION))
						.appPage()
						.section("tricomp-task-detail-actions", true, false)
						// Verify that the user is able to submit the work task
						.action(ShiftActionTypes.actionByLabel("Submit", ShiftActionTypes.SHIFT_APP))
						.click()
						.$(WorkTaskUXUtilV2.TAB("In Progress"))
						.field(ShiftConstants.searchField())
						.clear()
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(0)
						.click()
						// Test case 1067: Selenium V2 - automation - WTS - Verify if the worktask status is changed from Active to Completed
						.$(WorkTaskUXUtilV2.COMPLETE_WORK_TASK("Completed through automation"))
						.$(WorkTaskUXUtilV2.TAB("Completed"))
						.section("tricomp-task-list-tab-content")
						.appContent(ShiftAppPageContentTypes.SEARCH_LIST)
						.record(0)
						.click()
						.section("tricomp-task-detail-actions", true, false)
						// Test case 1069: Selenium V2 - automation - WTS - Verify if the worktask status is changed from Active - Complete -Close
						.action(ShiftActionTypes.actionByLabel("Close", ShiftActionTypes.SHIFT_APP))
						.click()
						.verify(ShiftVerifications.contains("Closed"))
						.$(WorkTaskUXUtilV2.NAVIGATE_TO_HOME())

						.header()
						.verify(ShiftVerifications.contains("Locate"))
						// Verify that the user is able to launch the Locate app from WTS UX portal.
						.menuItem("Locate")
						.click()
						.section("tricomp-search-person")
						.section("triapp-people-search")
						// Verify user is able to search for a certain person and verify the person details.
						.field(ShiftConstants.searchField())
						.enter("Locate User1")
						.section("tricomp-search-person")
						.section("triapp-people-search")
						.section("triblock-search")

						.appContent(ShiftAppPageContentTypes.SEARCH_TABLE)
						.row(0)
						.click()

						.verify(ShiftVerifications.notContains("No primary location is defined for this person."))

						.section("tricomp-person-details")
						.verify(ShiftVerifications.contains("Locate User1"))
						.verify(ShiftVerifications.hasIcon("ibm-glyphs:phone-call"))
						.verify(ShiftVerifications.hasIcon("ibm:mobile"))
						.verify(ShiftVerifications.hasIcon("ibm:email"))
						.section("triblock-image-info-card")
						.section("triplat-image")
						.verify(ShiftVerifications.hasImage())
						.appPage()

						.section("tricomp-location-details-person")
						.section("triblock-tabs")
						// Verify that once the user clicks on the Search result, 3 tabs should be displayed- Locations, Key Rooms, Floor Directory.
						.verify(ShiftVerifications.hasTab("Location", "Key Rooms", "Floor Directory"))
						// Verify that the location is having a floor plan
						.tab("Location")
						.click()
						.section("tricomp-location-details-person")
						.section("tripage-location")
						.section("triplat-graphic")
						.section("triplat-graphic-base")
						.verify(ShiftVerifications.notContains("Floor plan not available."))
						.appPage()
						.section("tricomp-location-details-person")
						.section("triblock-tabs")
						.tab("Key Rooms")
						.click()
						.section("tricomp-location-details-person")
						.section("tripage-key-rooms")
						.section("triplat-graphic-legend")
						.verify(ShiftVerifications.hasIcon("ibm-glyphs:back"))
						.appPage()

						.section("tricomp-location-details-person")
						.section("triblock-tabs")
						// Verify that under floor directory , the search user is displayed.
						.tab("Floor Directory")
						.click()
						.section("tricomp-location-details-person")
						.section("tripage-floor-directory")

						.verify(ShiftVerifications.contains("Locate User1"))

						.appPage()

						.logout();

			}
		});
	}
}
